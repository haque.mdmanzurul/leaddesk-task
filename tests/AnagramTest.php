<?php

declare(strict_types=1);

use LeadDeskClasses\AnagramAction;
use PHPUnit\Framework\TestCase;

class AnagramTest extends TestCase
{
    /** 
     * @covers AnagramAction::isAnagram
     */
    public function testAnagramAction(): void
    {
        $anagram = new AnagramAction();

        $result = $anagram->isAnagram('SDFSD', 'SDFDS');
        self::assertTrue($result);

        $result = $anagram->isAnagram('ANAGRAM', 'RAMANAG');
        self::assertTrue($result);

        $result = $anagram->isAnagram('ANAGRAM', 'nag-a-ram');
        self::assertFalse($result);

        $result = $anagram->isAnagram('ANAGRAM', 'NAGARAm');
        self::assertFalse($result);

        $result = $anagram->isAnagram('ANAGRAMb', 'RAMGANAb');
        self::assertTrue($result);
    }
}