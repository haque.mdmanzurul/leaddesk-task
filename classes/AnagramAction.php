<?php
namespace LeadDeskClasses;

use LeadDeskTasks\Anagram;

class AnagramAction implements Anagram
{
    /**
     * Find the second word is the anagram of first word
     * @param string $word1 first word
     * @param string $word2 second word
     * @return boolean true if word are anagrams of each other, false otherwise
     */
    public function isAnagram($word1, $word2): bool 
    {
        $word1 = str_split($word1);
        sort($word1);

        $word2 = str_split($word2);
        sort($word2);

        return $word1 === $word2;
    }
}