<?php
namespace LeadDeskClasses;

use LeadDeskTasks\MaxSubarray;

class MaxSubarrayAction implements MaxSubarray
{
    /**
     * Find the max sub array sum
     * @param array $input input values
     * @return int sum of the max sub array
     */
    public function contiguous($input): int 
    {
        $max_sum = array_sum($input);
        $all_negetive = 0;
        
        for($i = 0; $i < count($input); $i++) {
            if ($input[$i] > 0) {
                $all_negetive = false;
                break;
            }
            $all_negetive += abs($input[$i]);
        }  
        if ($all_negetive) return -1 * $all_negetive;

        for($i = 0; $i < count($input); $i++) {
            if (!empty($input[$i]) && $input[$i] > $max_sum) {
                $max_sum = $input[$i];
            }
            
            $child_arrays = array_chunk($input, $i + 1);
            for($j = 0; $j < count($child_arrays); $j++) {
                $sum = array_sum($child_arrays[$j]);
                if ($sum > $max_sum) $max_sum = $sum;
            }
        }
        return (int)$max_sum;
    }
}