
# LeadDesk Algorithm Test
A pretty simplified Docker Compose workflow that sets up a enviroment for simple php unit testing


## How to run
To run the test make sure you have the docker installed in your system and running.
1. After cloning the repo enter the repo folder and for the first time run below command to build and run the tests
```docker-compose run composer && docker-compose run phpunit tests```
If you already run the above command, then you can run ```docker-compose run phpunit tests```